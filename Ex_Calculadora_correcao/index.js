const { createApp } = Vue;

createApp({
    data(){
        return{
            display: "0",
            numAnterior: null,
            numAtual: null,
            operador: null
        }
    },
    methods: {
        handleButtonClick(valorBotao){
            switch(valorBotao){
                case "+":
                case "-":
                case "=":
                case "/":
                case "*":
                    this.handleOperador(valorBotao);
                    break;
                case ".":
                    this.handleDecimal();
                    break;
                case "=":
                    this.handleEquals();
                    break;
                default:
                    this.handleNumber(valorBotao);
                    break;
            }
        },

        handleNumber(numero){
            console.log("Está chamando o número: " + numero);
            if (this.display === "0"){
                this.display = numero.toString();
            } else {
                this.display += numero.toString();
            }
        },
        handleOperador(op){
            console.log("Está chamando o operador: " + op);
            if (this.numAnterior !== null){
                this.handleEquals();
            }
            
            this.operador = op;
            this.numAnterior = parseFloat(this.display);
            this.display = "0";
        },
        handleDecimal(){
            console.log("Está chamando o ponto decimal");
            if (!this.display.includes(".")) {
            this.display += "."
            }
        },
        handleEquals(){
            console.log("Está chamando o igual");
        }

    }
}).mount("#app");