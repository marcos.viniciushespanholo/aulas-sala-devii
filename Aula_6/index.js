const express = require("express");

const app = express();

app.use(express.json());  //compila o json para ser interpretado

app.listen(8080, () => {
    console.log("O servidor está ativo na porta 8080");
});

let Alunos = ['Maria', 'João', 'José', 'Fernanda'];

// READ
app.get('/getAluno', (req, res) => {
    const { index } = req.body;
    // connect SQL - SELECT * FROM Alunos WHERE id = index
    res.send(`<h1> O aluno ${Alunos [index]} foi encontrado! </h1>`); 
});


// C     R   U   D
// POST GET PUT DELETE

app.get('/', () => {
    console.log('O negocio deu CERTO!!!');
});

//  SELECT * FROM Alunos
app.get('/getAlunos', (req, res) => {
    console.log(Alunos);
    res.send(`Os alunos cadastrados são: ${Alunos}`);
});

// UPDATE
app.put('/updateAluno', (req, res) => {
    // UPDATE nome FROM Alunos WHERE id = index;
    const {index, nome} = req.body;
    Alunos[index] = nome;
    res.send("<h1>O nome foi atualizado com sucesso!!</h1>");
});

// DELETE
app.delete('/deleteAluno', (req, res) => {
    // DELETE FROM Alunos WHERE id = index;
    const {index} = req.body;
    Alunos.splice(index, 1);
    res.send("<h1>O nome foi deletado com sucesso!!</h1>");
    console.log(Alunos);
});