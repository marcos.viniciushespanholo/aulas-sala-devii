const { createApp } = Vue;

createApp({
    data(){
        return{
            isMsgAtiva: false,
        }
    },
    methods: {
        toggleMsg: function() {
            this.isMsgAtiva = !this.isMsgAtiva;
        }
    }
}).mount("#app");