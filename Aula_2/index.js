const express = require ("express")

const app = express()

app.listen(8080, () => {console.log("O server está ativo na porta 8080")})

let nome = "Marcos"

app.get("/", (req, res) => {
    res.send(`<h1>Olá ${nome}!!!</h1>`)
})

app.get("/getHTML", (req, res) => {
    const nome = req.body.nome
    console.log("Olá ${nome}")
})